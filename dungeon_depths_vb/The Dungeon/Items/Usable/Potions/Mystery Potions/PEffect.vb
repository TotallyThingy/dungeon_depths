﻿Public MustInherit Class PEffect
    'PEffect are applied as a result of using a potion
    Public MustOverride Sub apply(ByRef p As Player)
End Class
