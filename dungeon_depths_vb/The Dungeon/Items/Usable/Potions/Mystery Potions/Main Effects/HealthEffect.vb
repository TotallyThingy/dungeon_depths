﻿Public Class HealthEffect
    Inherits PEffect

    Public Overrides Sub apply(ByRef p As Player)
        Dim out = Game.lblEvent.Text.Split(vbCrLf)(0) & vbCrLf & vbCrLf

        p.health += 75 / p.getMaxHealth
        If p.health > 1 Then p.health = 1
        out += "+75 health."

        Game.pushLblEvent(out)
    End Sub
End Class
