﻿Public Class SportBra
    Inherits Armor
    Sub New()
        MyBase.setName("Sports_Bra")
        MyBase.setDesc("A sports bra made of a strechy matierial that allows it to fit many different bust sizes." & vbCrLf & _
                       "Fits sizes 1 through 4" & vbCrLf & _
                       "+1 DEF" & vbCrLf & _
                       "+5 SPD")
        id = 47
        tier = 3
        MyBase.setUsable(False)
        MyBase.dBoost = 1
        MyBase.sBoost = 5
        MyBase.count = 0
        MyBase.value = 400
        MyBase.bsize1 = New Tuple(Of Integer, Boolean, Boolean)(62, True, True)
        MyBase.bsize2 = New Tuple(Of Integer, Boolean, Boolean)(63, True, True)
        MyBase.bsize3 = New Tuple(Of Integer, Boolean, Boolean)(64, True, True)
        MyBase.bsize4 = New Tuple(Of Integer, Boolean, Boolean)(65, True, True)

        MyBase.usize1 = New Tuple(Of Integer, Boolean, Boolean)(129, True, True)
        MyBase.usize2 = New Tuple(Of Integer, Boolean, Boolean)(130, True, True)
        MyBase.usize3 = New Tuple(Of Integer, Boolean, Boolean)(131, True, True)
        MyBase.usize4 = New Tuple(Of Integer, Boolean, Boolean)(132, True, True)
        MyBase.usize5 = New Tuple(Of Integer, Boolean, Boolean)(133, True, True)
        MyBase.compressesBreasts = True
    End Sub
End Class
