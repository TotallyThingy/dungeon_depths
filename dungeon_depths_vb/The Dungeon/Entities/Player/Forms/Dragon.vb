﻿Public Class Dragon
    Inherits pForm
    Sub New()
        MyBase.New(1, 1.5, 1.5, 2, 0.5, 1, "Dragon", False)
        MyBase.revertPassage = "Your scales slowly disappear into your skin as you slowly turn back into a biped. On the bright side, you are pretty sure you could still breath fire if you really wanted to."
    End Sub
End Class
