﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProfilePictureMaster
{
    void regenerate_profile_picture();
    void update_armor();
    void update_body();
    void update_skin_color();
    void update_hair_color();
}
