﻿public interface IMessageMaster
{
    void display_message(string message);
    void set_message(string message);
}
